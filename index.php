<!DOCTYPE html>
<html>
<body>

<?php 
/**
 * DYNAMIC NAPBOTS ALLOCATION
 * (beta version, please double-check if its working properly)
 * This work is a fork of https://github.com/thibault-ketterer/napbots-weather-allocations
 * designed for google cloud deployement by Fred.Dupont
 */

if (file_exists('param.json')){
	$string = file_get_contents("param.json");
    $json = json_decode($string, true);
}
else {
    echo 'File param.json not found';
    exit;}
?>

<h1>Napbot Alloconfig</h1>

<form action="/update_param.php">
  <p>Bot Only:</p>
  <input type="radio" id="true" name="botonly" value="true" <?php if ($json['botonly']==='true'){echo 'checked';} ?>  >
  <label for="yes">Yes</label><br>
  <input type="radio" id="false" name="botonly" value="false" <?php if ($json['botonly']==='false'){echo 'checked';} ?> >
  <label for="no">No</label><br>
    <br>
    

  <label for="weather">Choose weather:</label>
  <select name="weather" id="weather">
    <option value="napbot" <?php if ($json['forced']==='no'){echo 'selected="selected"';} ?> >Napbot</option>
    <option value="mild_bull" <?php if ($json['weather']==='mild_bull'){echo 'selected="selected"';} ?>>Mild Bull</option>
    <option value="mild_bear" <?php if ($json['weather']==='mild_bear'){echo 'selected="selected"';} ?>>Mild Bear</option>
    <option value="extreme" <?php if ($json['weather']==='extreme'){echo 'selected="selected"';} ?>>Extreme</option>
  </select>
  <br><br>

  <input type="submit" value="METTRE A JOUR"><br>

  _____________________________________________
   
   <br> <br> <h1>Console</h1>

</form>

<?php

try {
	include 'config.php';
	if ($verbose) echo "[config.php] imported\n";
} catch (Exception $e) {
	exit("[config.php] not imported: " . $e->getMessage() . ". <br>");
}

/**
 * Script (do not touch here)
 */

$debug = false;
$verbose = true;
$dry_run = false;
$exchange_ignore_list = [];


function get_weather() {
	// Get crypto weather
	$weather = ''; $ts_max = '';
	for ($i = 1; $i <= 10; $i++) {
		$weatherApi = file_get_contents('https://middle.napbots.com/v1/crypto-weather');
		if ($weatherApi) {
			$data = json_decode($weatherApi, true)['data']['weather'];
			$ts = $data['ts'];
			if ($ts > $ts_max) {
				$weather = $data['weather'];
				$ts_max = $ts;
			}
		}
		usleep(250000);
	}
	return $weather;
}

function assign_composition($weather){
	$compositionToSet = null;
	global $compositions;
	global $debug;
	// Find composition to set
	if($debug) echo "enter assign_composition [$weather] <br>";
	if($weather === 'Extreme markets') {
		$compositionToSet = $compositions['extreme'];
	} elseif($weather === 'Mild bull markets'){
		$compositionToSet = $compositions['mild_bull'];
	} elseif($weather === 'Mild bear or range markets') {
		$compositionToSet = $compositions['mild_bear'];
	} else {
		echo 'Invalid crypto-weather: ' . $weather;
	}
	if($debug) echo "OUT assign_composition [$compositionToSet] <br>";
	if($debug) var_dump($compositionToSet);
	return $compositionToSet;
}

function usage(){
	echo "possible args are: <br> <br>";
	echo "force <br>";
	echo "extreme <br>";
	echo "mild_bull <br>";
	echo "mild_bear <br>";
	echo "dry <br>";
	echo "verbose <br>";
	echo "debug <br>";
}
function handle_args(){
	global $argv;
	global $verbose;
	global $forced_market;
	global $dry_run;
	global $debug;
	global $json;
	
	if (file_exists('param.json')){
		$string = file_get_contents("param.json");
		$json = json_decode($string, true);
	}
	
	$force = $json['forced'];

	if ($force==='true')  {
		$forced_market = $json['weather'];
	}

	$copy_argv = $argv;
	if(count($copy_argv) > 1){
		if ($debug) var_dump($copy_argv);
		array_shift($copy_argv); # remove $0

		foreach($copy_argv as $arg){
			switch ($arg) {
			case "force": $force = true; break;
			case "extreme": $forced_market = "extreme"; break;
			case "custom": $forced_market = "custom"; break;
			case "mild_bull": $forced_market = "mild_bull"; break;
			case "mild_bear": $forced_market = "mild_bear"; break;
			case "dry": $dry_run = true; break;
			case "verbose": $verbose = true; break;
			case "debug": $verbose = true; $debug = true; break;
			default:
				echo "unkwon arg [$arg] <br> <br>";
				usage();
				exit(1);
				break;
			}
		}
	}
	if(!$force) $forced_market = "";
}

function check_compositions($compositions) {
	global $verbose;
	global $debug;
	foreach ($compositions as $weather => $composition) {
		$sum = 0.0;
		foreach ($composition['compo'] as $label => $percentage) {
			$sum += abs(round($percentage, 3)); // an inverted strat has a negative value
			if ($debug) printf("[$weather] sum: %.3f\n", $sum);
		}
		if (round($sum, 3) != 1)
			echo "sum of you allocations for [$weather] is [$sum] it should be [1], check your numbers. <br>";
			exit(1);
		if ($composition['leverage'] < 0 or $composition['leverage'] > 1.5)
			echo "The leverage of the allocation for [$weather] is [" . $composition['leverage'] . "] it should be between 0.00 and 1.50. <br>";
			exit(1);
	}
	if ($verbose) echo "composition sums are OK (1). <br>";
}


$forced_market = "";
handle_args();

check_compositions($compositions);
if ($forced_market === '') {
	$weather = get_weather();
	$compositionToSet = assign_composition($weather);
} else {
	$weather = "FORCED";
	$compositionToSet = $compositions[$forced_market];
}

if ($verbose){
    echo "weather          [$weather] <br>";
	echo "compositionToSet [$compositionToSet] <br>";
	if($debug) var_dump($compositionToSet);
}

if (! is_array($compositionToSet)) {
	echo "error [$compositionToSet] is not an array, script is broken somewhere CANCELING EVERYTHING. <br>Account left untouched. <br>";
	exit(1);
}

// Log
echo "Crypto-weather is: " . $weather . " <br>";


// Get authToken
echo "authentication to napbots.... <br>";
$url = "https://middle.napbots.com/v1/user/login";
$headers =  "Content-Type: application/json\r \n";
$postData = json_encode(['email' => $email, 'password' => $password]);
$context = array('http' => array( 'method' => 'POST', 'header' => $headers, 'content' => $postData));
$context = stream_context_create($context);
$result = file_get_contents($url, false, $context);

$authToken = json_decode($result, true)['data']['accessToken'];
if(!$authToken) {
	echo "Unable to get auth token. <br> <br>DOUBLE CHECK YOUR CREDENTIALS login / password. <br> <br>";
}

echo "auth napbot OK <br>";

// Get userId
$url = "https://middle.napbots.com/v1/user/me";
$headers =  ['Content-Type: application/json', 'token: ' . $authToken];
$context = array('http' => array( 'method' => 'GET', 'header' => $headers));
$context = stream_context_create($context);
$result2 = file_get_contents($url, false, $context);
if ($result2 === "") {
	echo "Wrong userid ? <br>Unable to retrive userId <br>DOUBLE CHECK YOUR USERID. <br> <br>";
}

echo "userId retrieval OK <br>";
$userId = json_decode($result2, true)['data']['userId'];

// Get account infos
$url = 'https://middle.napbots.com/v1/account/for-user/' . $userId;
$headers =  ['Content-Type: application/json', 'token: ' . $authToken];
$context = array('http' => array( 'method' => 'GET', 'header' => $headers));
$context = stream_context_create($context);
$result2 = file_get_contents($url, false, $context);
if ($result2 === "") {
	echo "Wrong userid ? <br>Unable to retrive account infos <br>DOUBLE CHECK YOUR USERID. <br> <br>";
}

echo "user id infos retrieval OK <br>";
$data = json_decode($result2, true)['data'];

// Rebuild exchanges array
$exchanges = [];
$exchanges_names = [];
foreach($data as $exchange) {
	if (in_array($exchange['exchange'], $exchange_ignore_list)){
		echo "as requested, ignoring [".$exchange['exchange']."] <br>";
		continue;
	}

	if(empty($exchange['accountId'])) {
		echo 'no exchange found <br>';
	} else if (empty($exchange['compo'])) {
		var_dump($exchange);
		echo "Invalid exchange data for [".$exchange['exchange']."] <br> <br>";
	}

	$exchanges[$exchange['accountId']] = $exchange['compo'];
	$exchanges_names[$exchange['accountId']] = $exchange['exchange'];
}

// For each exchange, change allocation if different from crypto weather one
foreach($exchanges as $exchangeId => $exchange) {
	// If the leverage is different, set toUpdate
	$toUpdate = (floatval($exchange['leverage']) != floatval($compositionToSet['leverage']));

	// If composition different, set to update
	echo "<br><br>UNTIL NOW <br>";
	echo "<pre>";
	echo print_r($exchange['compo'],true);
	echo "</pre>";
	echo "<br>";
	if ($exchange['compo'] != $compositionToSet['compo']) {
		$toUpdate = true;
		if ($verbose){
			echo "<br><br>NEW <br>";
			echo print_r($compositionToSet['compo'], true);
			echo "<br>";
		}
	}

	// If composition different, update allocation for this exchange
	if(! $toUpdate) {
		// Log
		echo "Nothing to update for exchange " . $exchanges_names[$exchangeId] . " ". $exchangeId . " <br>";
		continue;
	}

	// Rebuild string for composition - $compositionToSet['botOnly']
	$params = json_encode([
		'botOnly' =>  $json['botonly'],
		'compo' => [
			'leverage' => strval($compositionToSet['leverage']),
			'compo' => $compositionToSet['compo']
		]
	]);

	$url = 'https://middle.napbots.com/v1/account/' . $exchangeId;
	$headers =  ['Content-Type: application/json', 'token: ' . $authToken];
	$context = array('http' => array( 'method' => 'PATCH', 'header' => $headers, 'content' => $params));

	if (! $dry_run){
		$context = stream_context_create($context);
		$result = file_get_contents($url, false, $context);
		// Log
		echo "Updated allocation for exchange " . $exchanges_names[$exchangeId] . " " . $exchangeId . " <br>";
	}
	else{
		echo "DRY RUN MODE <br>";
		echo "nothing was done to your account <br>";
	}
}

?>
</body>
</html>
